module gitlab.com/logius/cloud-native-overheid/tools/kibana-cli

go 1.18

require (
	github.com/elastic/go-elasticsearch/v8 v8.3.0
	github.com/google/go-cmp v0.6.0
	github.com/pkg/errors v0.9.1
	github.com/sethvargo/go-password v0.2.0
	github.com/spf13/cobra v1.3.0
	github.com/xanzy/go-gitlab v0.59.0
	gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli v0.0.0-20210409120209-2cf55852f3a4
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/elastic/elastic-transport-go/v8 v8.1.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/oauth2 v0.0.0-20220309155454-6242fa91716a // indirect
	golang.org/x/time v0.0.0-20220224211638-0e9765cccd65 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
