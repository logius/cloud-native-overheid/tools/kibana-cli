package elastic

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
	"github.com/pkg/errors"
)

func CreateElasticClient(elasticURL string) (*elasticsearch.Client, error) {
	elasticUser := os.Getenv("ELASTIC_USERNAME")
	elasticPassword := os.Getenv("ELASTIC_PASSWORD")

	if elasticUser == "" {
		return nil, errors.Errorf("Missing environment variable ELASTIC_USERNAME")
	}
	if elasticPassword == "" {
		return nil, errors.Errorf("Missing environment variable ELASTIC_PASSWORD")
	}
	cfg := elasticsearch.Config{
		Addresses: []string{elasticURL},
		Username:  elasticUser,
		Password:  elasticPassword,
	}
	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("error creating the elastic client: %s", err)
	}

	elasticInfo(es)
	return es, nil
}

func elasticInfo(es *elasticsearch.Client) {

	var r map[string]interface{}

	res, err := es.Info()
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()
	// Check response status
	if res.IsError() {
		log.Fatalf("Error: %s", res.String())
	}
	// Deserialize the response into a map.
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	// Print client and server version numbers.
	log.Printf("Elastic Client: %s", elasticsearch.Version)
	log.Printf("Elastic Server: %s", r["version"].(map[string]interface{})["number"])
}
