package elastic

import (
	"github.com/google/go-cmp/cmp"
	"testing"
)

func Test_defineServiceAccounts(t *testing.T) {
	type args struct {
		customerName              string
		serviceAccountSuffix      string
		skipFluentdServiceAccount bool
	}

	tests := []struct {
		name string
		args args
		want []ServiceAccountData
	}{
		{
			name: "test1",
			args: args{
				customerName:              "customer-1",
				serviceAccountSuffix:      "_TEST",
				skipFluentdServiceAccount: false,
			},
			want: []ServiceAccountData{
				{
					Name:             "customer-1_readonly_service_account",
					ClusterPerms:     []string{"monitor", "manage_own_api_key"},
					IndexPerms:       []string{"read", "view_index_metadata"},
					ApplicationPerms: []string{"feature_savedObjectsManagement.read"},
					GroupVarName:     "SP_ELASTIC_READONLY_USER_NAME_TEST",
					GroupVarPass:     "SP_ELASTIC_READONLY_PASSWORD_TEST",
				},
				{
					Name:             "customer-1_edit_service_account",
					ClusterPerms:     []string{"monitor", "manage_own_api_key"},
					IndexPerms:       []string{"read", "view_index_metadata"},
					ApplicationPerms: []string{"feature_savedObjectsManagement.all"},
					GroupVarName:     "SP_ELASTIC_EDIT_USER_NAME_TEST",
					GroupVarPass:     "SP_ELASTIC_EDIT_PASSWORD_TEST",
				},
				{
					Name:             "customer-1_fluentd_service_account",
					ClusterPerms:     []string{"monitor"},
					IndexPerms:       []string{"view_index_metadata", "write"},
					ApplicationPerms: nil,
					GroupVarName:     "SP_ELASTIC_USER_NAME_TEST",
					GroupVarPass:     "SP_ELASTIC_PASSWORD_TEST",
				},
			},
		},
		{
			name: "test2",
			args: args{
				customerName:              "customer-2",
				serviceAccountSuffix:      "_PROD",
				skipFluentdServiceAccount: false,
			},
			want: []ServiceAccountData{
				{
					Name:             "customer-2_readonly_service_account",
					ClusterPerms:     []string{"monitor", "manage_own_api_key"},
					IndexPerms:       []string{"read", "view_index_metadata"},
					ApplicationPerms: []string{"feature_savedObjectsManagement.read"},
					GroupVarName:     "SP_ELASTIC_READONLY_USER_NAME_PROD",
					GroupVarPass:     "SP_ELASTIC_READONLY_PASSWORD_PROD",
				},
				{
					Name:             "customer-2_edit_service_account",
					ClusterPerms:     []string{"monitor", "manage_own_api_key"},
					IndexPerms:       []string{"read", "view_index_metadata"},
					ApplicationPerms: []string{"feature_savedObjectsManagement.all"},
					GroupVarName:     "SP_ELASTIC_EDIT_USER_NAME_PROD",
					GroupVarPass:     "SP_ELASTIC_EDIT_PASSWORD_PROD",
				},
				{
					Name:             "customer-2_fluentd_service_account",
					ClusterPerms:     []string{"monitor"},
					IndexPerms:       []string{"view_index_metadata", "write"},
					ApplicationPerms: nil,
					GroupVarName:     "SP_ELASTIC_USER_NAME_PROD",
					GroupVarPass:     "SP_ELASTIC_PASSWORD_PROD",
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := defineServiceAccounts(tt.args.customerName, tt.args.serviceAccountSuffix, tt.args.skipFluentdServiceAccount); !cmp.Equal(got, tt.want) {
				t.Errorf(cmp.Diff(tt.want, got))
			}
		})
	}
}
