package elastic

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
)

const policyName = "jaeger-default-ilm-policy"
const jaegerVersion = "v1.28.0"

func createJaegerIndexes(es *elasticsearch.Client, elasticSearchConfig *config.CfgElastic) error {

	if err := createLifecyclePolicy(es, policyName); err != nil {
		return err
	}

	for _, jaegerPrefix := range elasticSearchConfig.JaegerPrefixes {
		log.Printf("Prepare indexes for jaeger prefix %q", jaegerPrefix)
		rolloverIndices := rolloverIndices(false, jaegerPrefix)
		for _, indexopt := range rolloverIndices {
			if err := createComponentTemplate(es, indexopt.Mapping); err != nil {
				return err
			}
			if err := createIndexTemplate(es, jaegerPrefix, indexopt.Mapping, policyName); err != nil {
				return err
			}
			if err := createInitialIndexWithAliases(es, indexopt); err != nil {
				return err
			}
		}
	}
	return nil
}

func createInitialIndexWithAliases(es *elasticsearch.Client, indexopt IndexOption) error {
	initialIndex := indexopt.InitialRolloverIndex()
	readAlias := indexopt.ReadAliasName()
	writeAlias := indexopt.WriteAliasName()

	if err := createIndexIfNotExist(es, indexopt.IndexName(), initialIndex); err != nil {
		return err
	}

	if err := createAliasIfNotExist(es, initialIndex, readAlias, false); err != nil {
		return err
	}

	if err := createAliasIfNotExist(es, initialIndex, writeAlias, true); err != nil {
		return err
	}

	return nil
}

func createIndexIfNotExist(es *elasticsearch.Client, indexBase string, initialIndex string) error {
	res, err := es.Indices.Get([]string{indexBase + "*"})
	defer res.Body.Close()
	if err != nil {
		return err
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}
	var result map[string]interface{}
	if err := json.Unmarshal([]byte(body), &result); err != nil {
		return err
	}

	if len(result) > 0 && res.StatusCode == 200 {
		log.Printf("Index %q already exists", indexBase+"*")
		return nil
	}

	log.Printf("Create Index %q", initialIndex)
	res, err = es.Indices.Create(initialIndex)
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}
	return nil
}

func createAliasIfNotExist(es *elasticsearch.Client, initialIndex string, alias string, isWriteIndex bool) error {
	res, err := es.Indices.GetAlias(es.Indices.GetAlias.WithName(alias))
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.StatusCode == 200 {
		log.Printf("Alias %q already exists", alias)
		return nil
	}
	log.Printf("Create Alias %q", alias)
	res, err = es.Indices.PutAlias([]string{initialIndex}, alias)
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}
	return nil
}

// TO make configurable on customer request
func createLifecyclePolicy(es *elasticsearch.Client, policyName string) error {

	body := strings.NewReader(fmt.Sprintf(`
	  {
	  "policy": {
		"phases": {
		  "hot": {
			"min_age": "0ms",
			"actions": {
			  "rollover": {
				"max_primary_shard_size": "50gb",
				"max_age": "1d"
			  },
			  "set_priority": {
				"priority": 100
			  }
			}
		  },
		  "warm": {
			"min_age": "1d",
			"actions": {
			  "forcemerge": {
				"max_num_segments": 1
			  },
			  "set_priority": {
				"priority": 50
			  },
			  "shrink": {
				"number_of_shards": 1
			  },
			  "allocate": {
				"number_of_replicas": 0
			  }
			}
		  },
		  "cold": {
			"min_age": "2d",
			"actions": {
			  "set_priority": {
				"priority": 25
			  }
			}
		  },
		  "delete": {
			"min_age": "3d",
			"actions": {
			  "delete": {
				"delete_searchable_snapshot": true
			  }
			}
		  }
		}
	  }
	}`))
	log.Printf("Create policy %q", policyName)
	res, err := es.ILM.PutLifecycle(policyName, es.ILM.PutLifecycle.WithBody(body))
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}

	return nil
}

func createComponentTemplate(es *elasticsearch.Client, mappingName string) error {
	mappings, err := getJaegerTemplateMapping(mappingName)
	if err != nil {
		return err
	}
	componentTemplateName := mappingName + "-mappings"

	body := strings.NewReader(fmt.Sprintf(`{
		"template": {
			"mappings": %v
		},
	}`, mappings))
	log.Printf("Create component template %q", componentTemplateName)
	res, err := es.Cluster.PutComponentTemplate(componentTemplateName, body)
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}

	return nil
}

func createIndexTemplate(es *elasticsearch.Client, indexPrefix string, mappingName string, policyName string) error {

	mappings, err := getJaegerTemplateMapping(mappingName)
	if err != nil {
		return err
	}
	templateName := indexPrefix + "-" + mappingName
	componentTemplateName := mappingName + "-mappings"

	body := strings.NewReader(fmt.Sprintf(`{
		"index_patterns": [
			"%s-*"
		],
		"composed_of": [ %q ],
		"priority": 16,
		"template": {
			"aliases": {
				"%s-read": {} 
			},
			"settings": {
				"number_of_shards": 1,
				"number_of_replicas": 0,
				"lifecycle.name": "%v",
				"lifecycle.rollover_alias": "%s-write"
			},
			"mappings": %v
		},
	}`, indexPrefix+"-"+mappingName,
		componentTemplateName,
		templateName,
		policyName,
		templateName,
		mappings))

	log.Printf("Create template %q", templateName)
	res, err := es.Indices.PutIndexTemplate(templateName, body)
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}

	return nil
}

func getJaegerTemplateMapping(mapping string) (string, error) {

	mappingTemplate := fmt.Sprintf("https://raw.githubusercontent.com/jaegertracing/jaeger/%s/plugin/storage/es/mappings/%s-7.json", jaegerVersion, mapping)
	resp, err := http.Get(mappingTemplate)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	parts := strings.Split(string(body), "\"mappings\":")
	if len(parts) < 2 {
		return "", fmt.Errorf("could not find mapping in jaeger template")
	}
	return parts[1], nil
}
