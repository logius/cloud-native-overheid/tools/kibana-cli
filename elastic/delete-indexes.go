package elastic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
)

type IndicesStats struct {
	Indices map[string]struct {
		UUID   string `json:"uuid"`
		Health string `json:"health"`
		Status string `json:"status"`
		Total  struct {
			Docs struct {
				Count   int `json:"count"`
				Deleted int `json:"deleted"`
			} `json:"docs"`
		} `json:"total"`
	} `json:"indices"`
}

type IndexLifecyleStatus struct {
	Indices map[string]struct {
		Index   string `json:"index"`
		Managed bool   `json:"managed"`
		Policy  string `json:"policy"`
		Phase   string `json:"phase"`
	} `json:"indices"`
}

func cleanupEmptyIndexes(es *elasticsearch.Client, dataStream DataStream) error {

	res, err := es.ILM.ExplainLifecycle(dataStream.Name)
	if err != nil {
		return err
	}
	var lifecycleStatus IndexLifecyleStatus
	parseResponse(res, &lifecycleStatus)

	for name, index := range lifecycleStatus.Indices {
		if index.Managed && index.Phase != "hot" {
			if err := deleteEmptyIndex(es, name); err != nil {
				return err
			}
		}
	}

	return nil
}

func deleteEmptyIndex(es *elasticsearch.Client, indexName string) error {
	// check with stats for count of docs in this index
	res, err := es.Indices.Stats(es.Indices.Stats.WithIndex(indexName), es.Indices.Stats.WithMetric("docs"))
	if err != nil {
		return err
	}

	var stats IndicesStats
	if err := parseResponse(res, &stats); err != nil {
		return err
	}

	for _, index := range stats.Indices {
		if index.Total.Docs.Count == 0 {
			log.Printf("Delete Empty Index %v", indexName)
			if _, err := es.Indices.Delete([]string{indexName}); err != nil {
				return err
			}
		}
	}
	return nil
}

func parseResponse(res *esapi.Response, response interface{}) error {
	defer res.Body.Close()
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}
	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("unable to perform action readAll: %w", err)
	}

	return json.Unmarshal(responseData, response)
}

