package elastic

import (
	"encoding/json"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/google/go-cmp/cmp"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
	"testing"
)

func Test_serviceAccountRoleBody(t *testing.T) {
	type ServiceAccountRoleBody struct {
		Cluster []string `json:"cluster"`
		Indices []struct {
			Names      []string `json:"names"`
			Privileges []string `json:"privileges"`
		} `json:"indices"`
		Applications []struct {
			Application string   `json:"application"`
			Privileges  []string `json:"privileges"`
			Resources   []string `json:"resources"`
		} `json:"applications"`
	}
	type args struct {
		es                            *elasticsearch.Client
		elasticSearchConfig           *config.CfgElastic
		serviceAcc                    ServiceAccountData
		publicDatastreams             []string
		skipAddingWildcardsInPatterns bool
	}
	tests := []struct {
		name string
		args args
		want ServiceAccountRoleBody
	}{
		{
			name: "test1",
			args: args{
				es: nil,
				elasticSearchConfig: &config.CfgElastic{
					CrossClusterReplication: false,
					DataStreams: []config.CfgDataStream{
						{
							Name: "ds-sp-demo-test-1",
						},
					},
					JaegerPrefixes: nil,
					KibanaSpaces: []config.KibanaSpace{
						{
							Name: "sp-demo-test-1-1",
						},
					},
					IngestPipelines: nil,
				},
				serviceAcc: ServiceAccountData{
					Name:             "customer-1_readonly_service_account",
					ClusterPerms:     []string{"monitor", "manage_own_api_key"},
					IndexPerms:       []string{"read", "view_index_metadata"},
					ApplicationPerms: []string{"feature_savedObjectsManagement.read"},
					GroupVarName:     "SP_ELASTIC_READONLY_USER_NAME_TEST",
					GroupVarPass:     "SP_ELASTIC_READONLY_PASSWORD_TEST",
				},
				publicDatastreams:             nil,
				skipAddingWildcardsInPatterns: false,
			},
			want: ServiceAccountRoleBody{
				Cluster: []string{
					"monitor",
					"manage_own_api_key",
				},
				Indices: []struct {
					Names      []string `json:"names"`
					Privileges []string `json:"privileges"`
				}{
					{
						Names:      []string{"ds-sp-demo-test-1*"},
						Privileges: []string{"read", "view_index_metadata"},
					},
				},
				Applications: []struct {
					Application string   `json:"application"`
					Privileges  []string `json:"privileges"`
					Resources   []string `json:"resources"`
				}{
					{
						Application: "kibana-.kibana",
						Privileges:  []string{"feature_savedObjectsManagement.read"},
						Resources: []string{
							"space:sp-demo-test-1-1",
						},
					},
				},
			},
		},
		{
			name: "test2",
			args: args{
				es: nil,
				elasticSearchConfig: &config.CfgElastic{
					CrossClusterReplication: false,
					DataStreams: []config.CfgDataStream{
						{
							Name: "ds-sp-demo-test-2",
						},
					},
					JaegerPrefixes: nil,
					KibanaSpaces: []config.KibanaSpace{
						{
							Name: "sp-demo-test-2-1",
						},
						{
							Name: "sp-demo-test-2-2",
						},
					},
					IngestPipelines: nil,
				},
				serviceAcc: ServiceAccountData{
					Name:             "customer-1_edit_service_account",
					ClusterPerms:     []string{"monitor", "manage_own_api_key"},
					IndexPerms:       []string{"read", "view_index_metadata"},
					ApplicationPerms: []string{"feature_savedObjectsManagement.all"},
					GroupVarName:     "SP_ELASTIC_EDIT_USER_NAME_TEST",
					GroupVarPass:     "SP_ELASTIC_EDIT_PASSWORD_TEST",
				},
				publicDatastreams:             nil,
				skipAddingWildcardsInPatterns: false,
			},
			want: ServiceAccountRoleBody{
				Cluster: []string{
					"monitor",
					"manage_own_api_key",
				},
				Indices: []struct {
					Names      []string `json:"names"`
					Privileges []string `json:"privileges"`
				}{
					{
						Names:      []string{"ds-sp-demo-test-2*"},
						Privileges: []string{"read", "view_index_metadata"},
					},
				},
				Applications: []struct {
					Application string   `json:"application"`
					Privileges  []string `json:"privileges"`
					Resources   []string `json:"resources"`
				}{
					{
						Application: "kibana-.kibana",
						Privileges:  []string{"feature_savedObjectsManagement.all"},
						Resources: []string{
							"space:sp-demo-test-2-1",
							"space:sp-demo-test-2-2",
						},
					},
				},
			},
		},
		{
			name: "test3",
			args: args{
				es: nil,
				elasticSearchConfig: &config.CfgElastic{
					CrossClusterReplication: false,
					DataStreams: []config.CfgDataStream{
						{
							Name: "ds-sp-demo-test-3",
						},
					},
					JaegerPrefixes: nil,
					KibanaSpaces: []config.KibanaSpace{
						{
							Name: "sp-demo-test-3-1",
						},
						{
							Name: "sp-demo-test-3-2",
						},
					},
					IngestPipelines: nil,
				},
				serviceAcc: ServiceAccountData{
					Name:             "customer-1_fluentd_service_account",
					ClusterPerms:     []string{"monitor"},
					IndexPerms:       []string{"view_index_metadata", "write"},
					ApplicationPerms: nil,
					GroupVarName:     "SP_ELASTIC_USER_NAME_TEST",
					GroupVarPass:     "SP_ELASTIC_PASSWORD_TEST",
				},
				publicDatastreams:             nil,
				skipAddingWildcardsInPatterns: false,
			},
			want: ServiceAccountRoleBody{
				Cluster: []string{
					"monitor",
				},
				Indices: []struct {
					Names      []string `json:"names"`
					Privileges []string `json:"privileges"`
				}{
					{
						Names:      []string{"ds-sp-demo-test-3*"},
						Privileges: []string{"view_index_metadata", "write"},
					},
				},
				Applications: []struct {
					Application string   `json:"application"`
					Privileges  []string `json:"privileges"`
					Resources   []string `json:"resources"`
				}{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := serviceAccountRoleBody(tt.args.es, tt.args.elasticSearchConfig, tt.args.serviceAcc, tt.args.publicDatastreams, tt.args.skipAddingWildcardsInPatterns)

			var body ServiceAccountRoleBody
			_ = json.Unmarshal([]byte(got), &body)
			if !cmp.Equal(tt.want, body) {
				t.Errorf(cmp.Diff(tt.want, body))
			}
		})
	}
}
