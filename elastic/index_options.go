package elastic

import (
	"fmt"
	"strings"
)

const writeAliasFormat = "%s-write"
const readAliasFormat = "%s-read"
const rolloverIndexFormat = "%s-000001"

// IndexOption holds the information for the indices to rollover
type IndexOption struct {
	prefix    string
	indexType string
	Mapping   string
}

// RolloverIndices return an array of indices to rollover
func rolloverIndices(archive bool, prefix string) []IndexOption {

	return []IndexOption{
		{
			prefix:    prefix,
			Mapping:   "jaeger-span",
			indexType: "jaeger-span",
		},
		{
			prefix:    prefix,
			Mapping:   "jaeger-service",
			indexType: "jaeger-service",
		},
	}
}

func (i *IndexOption) IndexName() string {
	return strings.TrimLeft(fmt.Sprintf("%s-%s", i.prefix, i.indexType), "-")
}

// ReadAliasName returns read alias name of the index
func (i *IndexOption) ReadAliasName() string {
	return fmt.Sprintf(readAliasFormat, i.IndexName())
}

// WriteAliasName returns write alias name of the index
func (i *IndexOption) WriteAliasName() string {
	return fmt.Sprintf(writeAliasFormat, i.IndexName())
}

// InitialRolloverIndex returns the initial index rollover name
func (i *IndexOption) InitialRolloverIndex() string {
	return fmt.Sprintf(rolloverIndexFormat, i.IndexName())
}

// TemplateName returns the prefixed template name
func (i *IndexOption) TemplateName() string {
	return strings.TrimLeft(fmt.Sprintf("%s-%s", i.prefix, i.Mapping), "-")
}
