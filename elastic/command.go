package elastic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/kibana"
)

type flags struct {
	clusterPrivileges             *[]string
	commonIndexes                 *[]string
	config                        *string
	dataStreamSuffix              *string
	elasticURL                    *string
	gitLabURL                     *string
	gitlabURLPeer                 *string
	kibanaURL                     *string
	oidcRealm                     *string
	production                    *bool
	publicDatastreams             *[]string
	remoteDataStreamSuffix        *string
	remoteElasticCluster          *string
	serviceAccountSuffix          *string
	skipAddingWildcardsInPatterns *bool
	skipFluentdServiceAccount     *bool
	standardRoles                 *bool
	rotatePassword                *bool
}

type ServiceAccountData struct {
	Name             string
	ClusterPerms     []string
	IndexPerms       []string
	ApplicationPerms []string
	GroupVarName     string
	GroupVarPass     string
}

type DataStream struct {
	Name    string `json:"name"`
	Indices []struct {
		IndexName string `json:"index_name"`
		IndexUUID string `json:"index_uuid"`
	} `json:"indices"`
	Generation         int    `json:"generation"`
	Status             string `json:"status"`
	Template           string `json:"template"`
	IlmPolicy          string `json:"ilm_policy"`
	Hidden             bool   `json:"hidden"`
	System             bool   `json:"system"`
	AllowCustomRouting bool   `json:"allow_custom_routing"`
	Replicated         bool   `json:"replicated"`
}

type DataStreamList struct {
	DataStreams []DataStream `json:"data_streams"`
}

// NewCommand creates a new command.
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureElasticService(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-elastic",
		Short: "Configure Elastic and Kibana stack",
		Long:  "This command configures Kibana spaces, Elastic service account and datastreams for a tenant.",
	}

	flags.clusterPrivileges = cmd.Flags().StringArray("cluster-privilege", []string{}, "Cluster privileges for the customer admin role")
	flags.commonIndexes = cmd.Flags().StringArray("common-index", nil, "Index for which the customer will get read access")
	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.elasticURL = cmd.Flags().String("elastic-url", "", "Elastic API endpoint")
	flags.gitLabURL = cmd.Flags().String("gitlab-url", "", "Gitlab API endpoint")
	flags.gitlabURLPeer = cmd.Flags().String("gitlab-url-peer", "", "URL of GitLab DR site")
	flags.dataStreamSuffix = cmd.Flags().String("datastream-suffix", "", "Suffix for name of datastream")
	flags.kibanaURL = cmd.Flags().String("kibana-url", "", "Kibana API endpoint")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.production = cmd.Flags().Bool("production", false, "Production site")
	flags.publicDatastreams = cmd.Flags().StringArray("public-datastreams", []string{}, "datastreams readable with all serviceaccounts")
	flags.remoteElasticCluster = cmd.Flags().String("remote-elastic-cluster", "", "Remote ElasticSearch cluster")
	flags.remoteDataStreamSuffix = cmd.Flags().String("remote-datastream-suffix", "", "Remote Datastream Suffix")
	flags.serviceAccountSuffix = cmd.Flags().String("service-account-suffix", "", "Suffix for service accountname in GitLab")
	flags.skipAddingWildcardsInPatterns = cmd.Flags().Bool("skip-adding-wildcards-in-patterns", false, "Do not add wildcards on index or datastream patterns")
	flags.skipFluentdServiceAccount = cmd.Flags().Bool("skip-fluentd-service-account", false, "Do not create fluentd service account")
	flags.standardRoles = cmd.Flags().Bool("standard_roles", false, "Create standard customer roles")
	flags.rotatePassword = cmd.Flags().Bool("rotate-password", false, "Rotate password of service account if it exists")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("elastic-url")
	cmd.MarkFlagRequired("gitlab-url")
	cmd.MarkFlagRequired("kibana-url")

	return cmd
}

func defineServiceAccounts(customerName string, serviceAccountSuffix string, skipFluentdServiceAccount bool) []ServiceAccountData {
	res := []ServiceAccountData{
		{
			Name:             customerName + "_readonly_service_account",
			ClusterPerms:     []string{"monitor", "manage_own_api_key"},
			IndexPerms:       []string{"read", "view_index_metadata"},
			ApplicationPerms: []string{"feature_savedObjectsManagement.read"},
			GroupVarName:     "SP_ELASTIC_READONLY_USER_NAME" + serviceAccountSuffix,
			GroupVarPass:     "SP_ELASTIC_READONLY_PASSWORD" + serviceAccountSuffix,
		},
		{
			Name:             customerName + "_edit_service_account",
			ClusterPerms:     []string{"monitor", "manage_own_api_key"},
			IndexPerms:       []string{"read", "view_index_metadata"},
			ApplicationPerms: []string{"feature_savedObjectsManagement.all"},
			GroupVarName:     "SP_ELASTIC_EDIT_USER_NAME" + serviceAccountSuffix,
			GroupVarPass:     "SP_ELASTIC_EDIT_PASSWORD" + serviceAccountSuffix,
		},
	}

	if !skipFluentdServiceAccount {
		fluentdaccount := ServiceAccountData{
			Name:         customerName + "_fluentd_service_account",
			ClusterPerms: []string{"monitor"},
			IndexPerms:   []string{"view_index_metadata", "write"},
			GroupVarName: "SP_ELASTIC_USER_NAME" + serviceAccountSuffix,
			GroupVarPass: "SP_ELASTIC_PASSWORD" + serviceAccountSuffix,
		}
		return append(res, fluentdaccount)
	} else {
		log.Printf("Skip create \"%s_fluentd_service_account\"", customerName)
	}
	return res
}

func configureElasticService(cmd *cobra.Command, flags *flags) error {

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	if customerConfig.ElasticTest == nil && customerConfig.ElasticProd == nil {
		log.Printf("No ElasticSearch configuration found - skipping")
		return nil
	}

	es, err := CreateElasticClient(*flags.elasticURL)
	if err != nil {
		return err
	}

	kibanaClient, err := kibana.CreateClient(*flags.kibanaURL)
	if err != nil {
		return err
	}

	if !*flags.production && customerConfig.ElasticTest != nil {
		if err := configureAll(es, kibanaClient, customerConfig, customerConfig.ElasticTest, flags); err != nil {
			return err
		}
	} else if *flags.production && customerConfig.ElasticProd != nil {
		if err := configureAll(es, kibanaClient, customerConfig, customerConfig.ElasticProd, flags); err != nil {
			return err
		}
	}

	return nil
}

func configureAll(es *elasticsearch.Client, kibanaClient *kibana.Client, customerConfig *config.Customer, elasticSearchConfig *config.CfgElastic, flags *flags) error {

	// if CCR is set, delete existing datastreams without suffix
	// if elasticSearchConfig.CrossClusterReplication {
	// 	if err := deleteDataStreams(es, elasticSearchConfig.DataStreams); err != nil {
	// 		return err
	// 	}
	// }

	if err := createIndexTemplates(es, customerConfig.Name, elasticSearchConfig, *flags.dataStreamSuffix); err != nil {
		return err
	}
	if err := createDataStreams(es, elasticSearchConfig, *flags.dataStreamSuffix); err != nil {
		return err
	}
	if elasticSearchConfig.CrossClusterReplication {
		if *flags.remoteElasticCluster == "" {
			return fmt.Errorf("missing command line option 'remote-elastic-cluster'")
		}
		if err := createReplicationFollowers(es, customerConfig.Name, elasticSearchConfig, *flags.remoteElasticCluster, *flags.remoteDataStreamSuffix); err != nil {
			return err
		}
	}

	if err := createJaegerIndexes(es, elasticSearchConfig); err != nil {
		return err
	}

	for _, serviceAccData := range defineServiceAccounts(customerConfig.Name, *flags.serviceAccountSuffix, *flags.skipFluentdServiceAccount) {
		err := createServiceAccountRole(es, elasticSearchConfig, serviceAccData, *flags.publicDatastreams, *flags.skipAddingWildcardsInPatterns)
		if err != nil {
			return err
		}

		err = createServiceAccount(es, customerConfig, *flags.gitLabURL, *flags.gitlabURLPeer, serviceAccData, *flags.rotatePassword)
		if err != nil {
			return err
		}
	}

	if err := configureSpaces(es, kibanaClient, customerConfig, elasticSearchConfig, flags); err != nil {
		return err
	}

	return nil
}

func deleteDataStreams(es *elasticsearch.Client, datastreams []config.CfgDataStream) error {
	for _, datastream := range datastreams {

		res, err := es.Indices.GetDataStream(es.Indices.GetDataStream.WithName(datastream.Name))
		if err != nil {
			return err
		}
		if res.StatusCode == 200 {
			log.Printf("Delete existing datastream %q", datastream.Name)
			res, err = es.Indices.DeleteDataStream([]string{datastream.Name})
			defer res.Body.Close()
			if err != nil {
				return err
			}
			if res.IsError() {
				return fmt.Errorf("%v", res)
			}
		}
	}
	return nil
}

func configureSpaces(es *elasticsearch.Client, kibanaClient *kibana.Client, customerConfig *config.Customer, elasticSearchConfig *config.CfgElastic, flags *flags) error {

	log.Printf("Configure spaces for %q", customerConfig.Name)

	// Create the spaces with roles and rolemappings
	for _, space := range elasticSearchConfig.KibanaSpaces {
		if err := kibanaClient.CreateSpace(space.Name); err != nil {
			return err
		}

		// create configured index patterns for the space
		for _, pattern := range space.IndexPatterns {
			if err := kibanaClient.CreateIndexPattern(space.Name, pattern); err != nil {
				return err
			}
		}

		if *flags.standardRoles {
			if err := kibanaClient.ConfigureRolesWithStandardTemplate(es, &space, *flags.oidcRealm, *flags.commonIndexes, *flags.clusterPrivileges, *flags.publicDatastreams, *flags.skipAddingWildcardsInPatterns); err != nil {
				return err
			}
		} else {
			if err := kibanaClient.ConfigureRolesWithPrivileges(es, &space, *flags.oidcRealm, *flags.commonIndexes, *flags.clusterPrivileges, *flags.publicDatastreams, *flags.skipAddingWildcardsInPatterns); err != nil {
				return err
			}
		}
	}
	return nil
}

// create a template for every specified datastream
func createIndexTemplates(es *elasticsearch.Client, customerName string, elasticSearchConfig *config.CfgElastic, datastreamSuffix string) error {

	for _, datastream := range elasticSearchConfig.DataStreams {

		policyName := datastream.Policy
		if policyName == "" {
			policyName = "datastream_very_short"
		} else {
			if err := lifecyclePolicyExists(es, policyName); err != nil {
				return err
			}
		}

		datastreamName := datastream.Name
		if elasticSearchConfig.CrossClusterReplication {
			datastreamName += datastreamSuffix
		}

		// Metadata is added to the Index template in order to track ownership. We can't set metadata on a datastream.
		// Because DataStream and Index template have a one2one relation, we can still track ownership of the Datastream.
		body := strings.NewReader(fmt.Sprintf(`{
			"index_patterns": [
				"%s"
			],
			"composed_of": ["logs-mappings", "logs-settings"],
			"priority": 16,
			"data_stream": { },
			"template": {
				"settings": {
					"number_of_shards": 1,
					"number_of_replicas": 0,
					"lifecycle.name": "%s"
				}
			},
			"_meta": {
				"owner": "%s",
				"lastupdated": "%s"
			}
		}
	`, datastreamName, policyName, customerName, time.Now().Format("02-01-2006")))

		log.Printf("Create or Update template %q", datastreamName)
		res, err := es.Indices.PutIndexTemplate(datastreamName, body)
		defer res.Body.Close()
		if err != nil {
			return err
		}
		if res.IsError() {
			return fmt.Errorf("%v", res)
		}
	}
	return nil
}

func lifecyclePolicyExists(es *elasticsearch.Client, policyName string) error {
	res, err := es.ILM.GetLifecycle(es.ILM.GetLifecycle.WithPolicy(policyName))
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}
	return nil
}

func createReplicationFollowers(es *elasticsearch.Client, customerName string, elasticSearchConfig *config.CfgElastic, remoteCluster string, remoteDataStreamSuffix string) error {

	dsList := createRemoteDatastreamList(elasticSearchConfig, remoteDataStreamSuffix)

	log.Printf("Create/update auto follow pattern %q", strings.ReplaceAll(dsList, "\"", ""))

	// follow_index_pattern is empty as this cannot be used for datastream backing indices
	body := strings.NewReader(fmt.Sprintf(`
		{
			"remote_cluster": "%s", 
      		"leader_index_patterns": [ %s ],
			"follow_index_pattern": "{{leader_index}}" 
		}`, remoteCluster, dsList))
	res, err := es.CCR.PutAutoFollowPattern(customerName, body)
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}

	return nil
}

func createDataStreams(es *elasticsearch.Client, elasticSearchConfig *config.CfgElastic, streamSuffix string) error {
	for _, datastream := range elasticSearchConfig.DataStreams {
		datastreamName := datastream.Name
		if elasticSearchConfig.CrossClusterReplication {
			datastreamName += streamSuffix
		}
		res, err := es.Indices.GetDataStream(es.Indices.GetDataStream.WithName(datastreamName))
		defer res.Body.Close()
		if err != nil {
			return err
		}
		responseData, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return fmt.Errorf("unable to perform action readAll: %v", err)
		}
		if res.StatusCode == 200 {
			log.Printf("Datastream %q already exists", datastreamName)
			var dataStreamList DataStreamList
			if err := json.Unmarshal(responseData, &dataStreamList); err != nil {
				return err
			}
			if len(dataStreamList.DataStreams) > 0 {
				if err := cleanupEmptyIndexes(es, dataStreamList.DataStreams[0]); err != nil {
					return err
				}
			}
			continue
		}
		log.Printf("Create datastream %q", datastreamName)
		res, err = es.Indices.CreateDataStream(datastreamName)
		defer res.Body.Close()
		if err != nil {
			return err
		}
		if res.IsError() {
			return fmt.Errorf("%v", res)
		}
	}
	return nil
}

// optionally add Jaeger indexes to the permissions
func jaegerIndexesAsString(elasticSearchConfig *config.CfgElastic) string {
	var sb strings.Builder

	for n, jaegerPrefix := range elasticSearchConfig.JaegerPrefixes {
		if n > 0 {
			sb.WriteString(",")
		}
		sb.WriteString("\"")
		sb.WriteString(jaegerPrefix + "-jaeger-*")
		sb.WriteString("\"")
	}
	indexPermissions := fmt.Sprintf(`
			{
			"names": [ %s ],
			"privileges": [ "view_index_metadata", "read", "write" ]
			}
			`, sb.String())

	if sb.Len() > 0 {
		return ", " + indexPermissions
	}
	return ""
}

func publicDatastreamsAsString(es *elasticsearch.Client, publicDatastreams []string) string {

	var sb strings.Builder

	if len(publicDatastreams) > 0 {

		// allow public datastreams to be provided as CSV
		if strings.Contains(publicDatastreams[0], ",") {
			publicDatastreams = strings.Split(publicDatastreams[0], ",")
		}

		// if public datastream index exists, give service account access to it
		for n, i := range publicDatastreams {
			res, err := es.Indices.GetDataStream(es.Indices.GetDataStream.WithName(i))
			defer res.Body.Close()
			if err != nil {
				log.Println(err)
				return ""
			}
			if res.StatusCode == 200 {
				if n > 0 {
					sb.WriteString(",")
				}
				sb.WriteString("\"")
				sb.WriteString(i)
				sb.WriteString("\"")
			}
		}
	}

	indexPermissions := fmt.Sprintf(`
			{
			"names": [ %s ],
			"privileges": [ "view_index_metadata", "read"]
			}
			`, sb.String())

	if sb.Len() > 0 {
		return ", " + indexPermissions
	}
	return ""
}

func createIndexTemplateList(elasticSearchConfig *config.CfgElastic, skipAddingWildcardsInPatterns bool) string {
	// First create unique list of patterns
	var patterns = []string{}
	m := make(map[string]bool)
	// Append pattern for each datastream
	for _, datastream := range elasticSearchConfig.DataStreams {
		dspattern := datastream.Name
		if !skipAddingWildcardsInPatterns {
			dspattern = datastream.Name + "*"
		}
		if _, ok := m[dspattern]; !ok {
			m[dspattern] = true
			patterns = append(patterns, dspattern)
		}
	}
	log.Printf("createIndexTemplateList patterns             %q", patterns)
	// Append index patterns for each space
	for _, space := range elasticSearchConfig.KibanaSpaces {
		for _, pattern := range space.IndexPatterns {
			if _, ok := m[pattern]; !ok {
				m[pattern] = true
				patterns = append(patterns, pattern)
			}
		}
	}
	log.Printf("createIndexTemplateList +space.IndexPatterns %q", patterns)
	// Now convert the list in a comma-separated string (JSON array)
	var sb strings.Builder
	for _, pattern := range patterns {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString("\"" + pattern + "\"")
	}
	return sb.String()
}

func createRemoteDatastreamList(elasticSearchConfig *config.CfgElastic, remoteDataStreamSuffix string) string {
	var sb strings.Builder
	for _, datastream := range elasticSearchConfig.DataStreams {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString("\"")
		sb.WriteString(fmt.Sprintf(".ds-%s%s-*", datastream.Name, remoteDataStreamSuffix))
		sb.WriteString("\"")
	}
	return sb.String()
}

func elasticVarsDefinedInGitLab(gitLabClient *gitlab.Client, customerConfig *config.Customer, gitlabURL string, serviceAccountData ServiceAccountData) bool {

	for _, gitlabGroup := range customerConfig.GitLab.Groups {

		group, err := gitlabclient.GetGitLabGroup(gitLabClient, gitlabGroup.Name)
		if err != nil {
			return false
		}
		userName, _, _ := gitLabClient.GroupVariables.GetVariable(group.ID, serviceAccountData.GroupVarName)
		password, _, _ := gitLabClient.GroupVariables.GetVariable(group.ID, serviceAccountData.GroupVarName)

		if userName == nil || password == nil {
			return false
		}
	}
	return true
}

func saveElasticAccountInGitLab(gitLabClient *gitlab.Client, customerConfig *config.Customer, serviceAccountData ServiceAccountData, password string) error {

	groupVars := make(map[string]gitlabclient.GroupVariable)
	groupVars[serviceAccountData.GroupVarName] = gitlabclient.GroupVariable{
		Value:  serviceAccountData.Name,
		Masked: false,
	}
	groupVars[serviceAccountData.GroupVarPass] = gitlabclient.GroupVariable{
		Value:  password,
		Masked: false,
	}

	for _, gitlabGroup := range customerConfig.GitLab.Groups {
		if err := gitlabclient.UpdateVarsInGitLabGroup(gitLabClient, gitlabGroup.Name, groupVars); err != nil {
			return err
		}
	}
	return nil
}
