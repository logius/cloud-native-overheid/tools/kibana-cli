package elastic

import (
	"fmt"
	"log"
	"os"
	"strings"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
	"github.com/sethvargo/go-password/password"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
)

func createServiceAccountRole(es *elasticsearch.Client, elasticSearchConfig *config.CfgElastic, serviceAccountData ServiceAccountData, publicDatastreams []string, skipAddingWildcardsInPatterns bool) error {
	body := strings.NewReader(serviceAccountRoleBody(es, elasticSearchConfig, serviceAccountData, publicDatastreams, skipAddingWildcardsInPatterns))

	log.Printf("Create role %q", serviceAccountData.Name)
	res, err := es.Security.PutRole(serviceAccountData.Name, body)
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}
	return nil
}

func serviceAccountRoleBody(es *elasticsearch.Client, elasticSearchConfig *config.CfgElastic, serviceAcc ServiceAccountData, publicDatastreams []string, skipAddingWildcardsInPatterns bool) string {

	itList := createIndexTemplateList(elasticSearchConfig, skipAddingWildcardsInPatterns)

	return fmt.Sprintf(`{
		"cluster": [ "%s" ],
		"indices": [
			{
			"names": [ %s ],
			"privileges": [ "%s" ]
			}
			%v
			%v
		],
		%v
	  }
	  `, strings.Join(serviceAcc.ClusterPerms, `", "`),
		itList,
		strings.Join(serviceAcc.IndexPerms, `", "`),
		jaegerIndexesAsString(elasticSearchConfig),
		publicDatastreamsAsString(es, publicDatastreams),
		applicationsAsString(serviceAcc.ApplicationPerms, elasticSearchConfig.KibanaSpaces))
}

func applicationsAsString(applicationPerms []string, spaces []config.KibanaSpace) string {
	if len(applicationPerms) == 0 {
		return `"applications" : []`
	}
	var spaceNames []string
	for _, space := range spaces {
		spaceNames = append(spaceNames, "space:"+space.Name)
	}

	return fmt.Sprintf(`
		"applications" : [
			{
				"application" : "kibana-.kibana",
				"privileges" : [
					"%v"
				],
				"resources" : [
					"%v"
				]
			}
    	]
	`,
		strings.Join(applicationPerms, `", "`),
		strings.Join(spaceNames, `", "`))
}

func createServiceAccount(es *elasticsearch.Client, customerConfig *config.Customer, gitlabURL string, gitlabURLPeer string, serviceAccountData ServiceAccountData, rotatePassword bool) error {
	gitLabClient := gitlabclient.NewGitLabClient(gitlabURL)

	passwordGenerator, err := password.NewGenerator(&password.GeneratorInput{
		Symbols: "~@#()_-=|[]:<>",
	})
	if err != nil {
		return err
	}
	newPassword, _ := passwordGenerator.Generate(20, 5, 5, false, false)

	if elasticVarsDefinedInGitLab(gitLabClient, customerConfig, gitlabURL, serviceAccountData) {
		res, err := es.Security.GetUser(es.Security.GetUser.WithUsername(serviceAccountData.Name))
		defer res.Body.Close()
		if err != nil {
			return err
		}

		if res.StatusCode != 200 {
			return nil
		}
		log.Printf("Serviceaccount %q already exists in Elastic and GitLab", serviceAccountData.Name)
		if !rotatePassword {
			return nil
		}

		log.Printf("Rotating service account password")
		body := strings.NewReader(fmt.Sprintf(`{
					"password": "%s"
			  	}`, newPassword))
		res, err = es.Security.ChangePassword(body, es.Security.ChangePassword.WithUsername(serviceAccountData.Name))
		defer res.Body.Close()
		if err != nil {
			return err
		}
		if res.IsError() {
			return fmt.Errorf("%v", res)
		}

	} else {
		log.Printf("Creating new service account")
		body := strings.NewReader(fmt.Sprintf(`{
			"password": "%s",
			"full_name": "Service account for %s",
			"roles": [ "%s" ]
		}`, newPassword, customerConfig.Name, serviceAccountData.Name))
		res, err := es.Security.PutUser(serviceAccountData.Name, body)
		defer res.Body.Close()
		if err != nil {
			return err
		}
		if res.IsError() {
			return fmt.Errorf("%v", res)
		}
	}

	log.Printf("Saving Elastic account in GitLab")
	err = saveElasticAccountInGitLab(gitLabClient, customerConfig, serviceAccountData, newPassword)

	var gitlabClientPeer *gitlab.Client
	if gitlabURLPeer != "" {
		gitlabClientPeer, _ = gitlab.NewClient(os.Getenv("GITLAB_ACCESS_TOKEN"), gitlab.WithBaseURL(fmt.Sprintf("%s/api/v4", gitlabURLPeer)))
		err_dr := saveElasticAccountInGitLab(gitlabClientPeer, customerConfig, serviceAccountData, newPassword)
		if err_dr != nil {
			log.Printf("Ignoring error updating gitlab variables for the DR site. err= %v", err_dr)
		}
	}

	return err
}
