#!/bin/bash

IMAGE=registry.gitlab.com/logius/cloud-native-overheid/tools/kibana-cli:local

docker run --rm \
    -v $PWD/examples:/examples \
    $IMAGE configure-space --config=examples/tenant.config.yaml
