package kibana

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/pkg/errors"
)

// Content types
const jsonContentType = "application/json"

// Client struct for Kibana Rest API
type Client struct {
	FQDN     string
	Username string
	Password string
}

func CreateClient(kibanaURL string) (*Client, error) {
	kibanaURL = strings.TrimSuffix(kibanaURL, "/")
	kibanaUser := os.Getenv("ELASTIC_USERNAME")
	kibanaPassword := os.Getenv("ELASTIC_PASSWORD")

	if kibanaUser == "" {
		return nil, errors.Errorf("Missing environment variable ELASTIC_USERNAME")
	}
	if kibanaPassword == "" {
		return nil, errors.Errorf("Missing environment variable ELASTIC_PASSWORD")
	}
	client := &Client{
		Username: kibanaUser,
		Password: kibanaPassword,
		FQDN:     kibanaURL,
	}

	return client, nil
}

// doJSONRequest performs a REST request with the http library
func (kibanaClient *Client) doJSONRequest(method string, requestPath string, body *bytes.Buffer, response interface{}, allowedStatusCodes []int) error {

	url := fmt.Sprintf("%s/%s", kibanaClient.FQDN, requestPath)
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return err
	}
	req.SetBasicAuth(kibanaClient.Username, kibanaClient.Password)
	req.Header.Set("Content-Type", jsonContentType)
	req.Header.Set("kbn-xsrf", "true")

	var client = &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	//log.Printf("URL: %s, Method %s, Body: %s", url, method, buf)
	if !statusCodeOK(res.StatusCode, allowedStatusCodes) {
		responseData, _ := ioutil.ReadAll(res.Body)
		json, _ := json.Marshal(responseData)
		return errors.Errorf("URL: %s, Method %s, Body: %s, Error status code: %d responseData %v", url, method, json, res.StatusCode, string(responseData))
	}

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	//	log.Printf("URL: %s, Method %s, Response: %s", url, method, responseData)
	if len(responseData) > 0 && response != nil {
		//log.Printf("%v", string(responseData))
		err = json.Unmarshal(responseData, response)
		if err != nil {
			return errors.Errorf("URL: %s, Method %s, Body: %s, Response: %s", url, method, body, responseData)
		}
	}
	return nil
}

func objectToBuffer(object interface{}) *bytes.Buffer {
	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(object)
	return buf
}

func statusCodeOK(statusCode int, allowedStatuscodes []int) bool {
	for _, code := range allowedStatuscodes {
		if code == statusCode {
			return true
		}
	}
	return false
}
