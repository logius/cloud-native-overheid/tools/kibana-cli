package kibana

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/elastic/go-elasticsearch/v8"
)

// createRole create a Role 'roleName' with access to a space with name 'spaceName' and indices that match 'namespacePatterns'
func (kibanaClient *Client) createRole(spaceName string, roleName string, namespacePatterns []string, privileges []string, commonIndexes []string, clusterPrivileges []string, es *elasticsearch.Client, publicDatastreams []string, skipAddingWildcardsInPatterns bool) error {

	type FieldSecurity struct {
		Grant []string `json:"grant"`
	}
	type Index struct {
		Names         []string      `json:"names"`
		Privileges    []string      `json:"privileges"`
		FieldSecurity FieldSecurity `json:"field_security"`
	}
	type ElasticSearch struct {
		Cluster []string `json:"cluster"`
		Indices []Index  `json:"indices"`
	}
	type Feature struct {
		Actions                   []string `json:"actions"`
		AdvancedSettings          []string `json:"advancedSettings"`
		Apm                       []string `json:"apm"`
		Canvas                    []string `json:"canvas"`
		Dashboard                 []string `json:"dashboard"`
		DevTools                  []string `json:"dev_tools"`
		Discover                  []string `json:"discover"`
		FilesManagement           []string `json:"filesManagement"`
		FilesSharedImage          []string `json:"filesSharedImage"`
		GeneralCases              []string `json:"generalCases"`
		Graph                     []string `json:"graph"`
		GuidedOnboardingFeature   []string `json:"guidedOnboardingFeature"`
		IndexPatterns             []string `json:"indexPatterns"`
		Infrastructure            []string `json:"infrastructure"`
		Integrations              []string `json:"fleet"` // This feature has the name Integrations but id fleet in the API
		Logs                      []string `json:"logs"`
		MaintenanceWindow         []string `json:"maintenanceWindow"`
		Maps                      []string `json:"maps"`
		Ml                        []string `json:"ml"`
		ObservabilityAIAssistant  []string `json:"observabilityAIAssistant"`
		ObservabilityCases        []string `json:"observabilityCases"`
		Osquery                   []string `json:"osquery"`
		RulesSettings             []string `json:"rulesSettings"`
		SavedObjectsManagement    []string `json:"savedObjectsManagement"`
		SavedObjectsTagging       []string `json:"savedObjectsTagging"`
		SavedQueryManagement      []string `json:"savedQueryManagement"`
		SecuritySolutionAssistant []string `json:"securitySolutionAssistant"`
		SecuritySolutionCases     []string `json:"securitySolutionCases"`
		Siem                      []string `json:"siem"`
		Slo                       []string `json:"slo"`
		StackAlerts               []string `json:"stackAlerts"`
		Uptime                    []string `json:"uptime"`
		Visualize                 []string `json:"visualize"`
	}
	type KibanaSpace struct {
		Feature Feature `json:"feature"`
		Spaces []string `json:"spaces"`
	}
	type Role struct {
		ElasticSearch ElasticSearch `json:"elasticsearch"`
		Kibana        []KibanaSpace `json:"kibana"`
	}

	// add wildcards to namespaces to support multiple suffixes in the namespace pattern
	namespacePatternsWildCard := make([]string, 0)
	for _, namespacePattern := range namespacePatterns {
		if !skipAddingWildcardsInPatterns {
			if !strings.HasSuffix(namespacePattern, "*") {
				namespacePattern += "*"
			}
		}
		namespacePatternsWildCard = append(namespacePatternsWildCard, namespacePattern)
	}

	indices := []Index{
		{
			Names:         namespacePatternsWildCard,
			Privileges:    privileges,
			FieldSecurity: FieldSecurity{Grant: []string{"*"}},
		},
	}
	log.Printf("createRole indices            %q", indices)

	if len(commonIndexes) > 0 {
		commonIndexes := Index{
			Names:         commonIndexes,
			Privileges:    []string{"read", "view_index_metadata"},
			FieldSecurity: FieldSecurity{Grant: []string{"*"}},
		}
		indices = append(indices, commonIndexes)
	}
	log.Printf("createRole +commonIndexes     %q", indices)

	if len(publicDatastreams) > 0 {
		publicDatastreams := Index{
			Names:         publicDatastreamsAsStringArray(publicDatastreams),
			Privileges:    []string{"read", "view_index_metadata"},
			FieldSecurity: FieldSecurity{Grant: []string{"*"}},
		}
		indices = append(indices, publicDatastreams)
	}
	log.Printf("createRole +publicDatastreams %q", indices)

	roleDefinition := Role{
		ElasticSearch: ElasticSearch{
			Cluster: clusterPrivileges,
			Indices: indices,
		},
		Kibana: []KibanaSpace{
			{
				Feature: Feature{
					Actions:                   []string{"all"},
					AdvancedSettings:          []string{"all"},
					Apm:                       []string{"all"},
					Canvas:                    []string{"all"},
					Dashboard:                 []string{"all"},
					DevTools:                  []string{"all"},
					Discover:                  []string{"all"},
					FilesManagement:           []string{"all"},
					FilesSharedImage:          []string{"all"},
					GeneralCases:              []string{"all"},
					Graph:                     []string{"all"},
					GuidedOnboardingFeature:   []string{"all"},
					IndexPatterns:             []string{"all"},
					Infrastructure:            []string{"all"},
					Integrations:              []string{"read"},
					Logs:                      []string{"all"},
					MaintenanceWindow:         []string{"all"},
					Maps:                      []string{"all"},
					Ml:                        []string{"all"},
					ObservabilityAIAssistant:  []string{"all"},
					ObservabilityCases:        []string{"all"},
					Osquery:                   []string{"all"},
					RulesSettings:             []string{"all"},
					SavedObjectsManagement:    []string{"all"},
					SavedObjectsTagging:       []string{"all"},
					SavedQueryManagement:      []string{"all"},
					SecuritySolutionAssistant: []string{"all"},
					SecuritySolutionCases:     []string{"all"},
					Siem:                      []string{"all"},
					Slo:                       []string{"all"},
					StackAlerts:               []string{"all"},
					Uptime:                    []string{"all"},
					Visualize:                 []string{"all"},
				},
				Spaces: []string{spaceName},
			},
		},
	}

	roleDefinitionBytes, err := json.Marshal(roleDefinition)
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	buf.Write(roleDefinitionBytes)
	log.Printf("Create/update role %q for space %q to see the patterns %v with privileges %v", roleName, spaceName, namespacePatterns, privileges)
	if len(commonIndexes) > 0 {
		log.Printf("Update role %q for space %q to view the common indexes %v", roleName, spaceName, commonIndexes)
	}
	request := fmt.Sprintf("api/security/role/%s", roleName)
	return kibanaClient.doJSONRequest(http.MethodPut, request, buf, nil, []int{204})
}

func publicDatastreamsAsStringArray(publicDatastreams []string) []string {

	// allow public datastreams to be provided as CSV
	if strings.Contains(publicDatastreams[0], ",") {
		publicDatastreams = strings.Split(publicDatastreams[0], ",")
	}

	return publicDatastreams
}

// createRoleMapping creates a RoleMapping with name 'roleName' for OIDC groups 'groupNames' to roles 'roleName'
func createRoleMapping(es *elasticsearch.Client, oidcRealm string, roleName string, groupNames []string) error {

	roleMappingTemplate := `
	{
		"enabled": true,
		"roles": [ "%s" ], /* Elastic Kibana roles */
		"rules": {
			"all": [
				{
					"field": { "realm.name": "%s" }  /* OIDC realm */
				},
				{
					"field": { "groups": [%s] } /* OIDC groups */
				}
			]
		}
	}
	`

	log.Printf("Create/update rolemapping %q for %q", roleName, groupNames)
	body := strings.NewReader(fmt.Sprintf(roleMappingTemplate, roleName, oidcRealm, joinNamesArray(groupNames)))

	res, err := es.Security.PutRoleMapping(roleName, body)
	defer res.Body.Close()
	if err != nil {
		return err
	}
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}
	return nil
}

// joinNamesArray joins the strings in the given slice, quoted and separated by a comma
func joinNamesArray(names []string) string {
	return `"` + strings.Join(names, `","`) + `"`
}
