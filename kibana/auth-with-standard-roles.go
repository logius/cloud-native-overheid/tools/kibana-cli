package kibana

import (
	"log"

	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
)

// This method supports standard roles: viewer and admin.
func (kibanaClient *Client) ConfigureRolesWithStandardTemplate(es *elasticsearch.Client, space *config.KibanaSpace, oidcRealm string, commonIndexes []string, clusterPrivileges []string, publicDatastreams []string, skipAddingWildcardsInPatterns bool) error {

	// loop over roles within the space and create roles and rolemappings accordingly
	for _, role := range space.Roles {
		roleName := space.Name + "-" + role.RoleName
		privileges := getPrivileges(role.RoleName)

		if len(privileges) == 0 {
			log.Printf("Error: role %q is not supported", role.RoleName)
		} else {
			err := kibanaClient.createRole(space.Name, roleName, space.IndexPatterns, privileges, commonIndexes, clusterPrivileges, es, publicDatastreams, skipAddingWildcardsInPatterns)
			if err != nil {
				return err
			}

			if oidcRealm != "" {
				err := createRoleMapping(es, oidcRealm, roleName, role.OidcGroups)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func getPrivileges(roleName string) []string {
	if roleName == "viewer" {
		return []string{
			"read",
		}
	}
	if roleName == "admin" {
		return []string{
			"read",
			"manage",
		}
	}
	return make([]string, 0)
}
