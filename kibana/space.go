package kibana

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
)

// Space represents a Kibana space
type Space struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// SavedObject represents a Kibana SavedObject
type SavedObject struct {
	Attributes map[string]string `json:"attributes"`
}

// SavedObjects represents a list of Kibana SavedObject
type SavedObjects struct {
	SavedObjects []SavedObject `json:"saved_objects"`
}

// createSpace creates a space for a customer
func (kibanaClient *Client) CreateSpace(customerName string) error {

	existingSpace := Space{}
	if err := kibanaClient.doJSONRequest(http.MethodGet, fmt.Sprintf("api/spaces/space/%s", customerName), new(bytes.Buffer), &existingSpace, []int{200, 404}); err != nil {
		return err
	}

	if existingSpace.ID != "" {
		log.Printf("Skip create space %q as it already exists", customerName)
		return nil
	}

	log.Printf("Create space %q", customerName)
	newSpace := Space{
		ID:   customerName,
		Name: customerName,
	}

	return kibanaClient.doJSONRequest(http.MethodPost, "api/spaces/space", objectToBuffer(newSpace), nil, []int{200})
}

// createIndexPattern creates an Index Pattern in a space
func (kibanaClient *Client) CreateIndexPattern(spaceName string, pattern string) error {
	request := fmt.Sprintf("s/%s/api/saved_objects/_find?per_page=1000&type=index-pattern&fields=title", spaceName)
	savedObjects := SavedObjects{}
	if err := kibanaClient.doJSONRequest(http.MethodGet, request, new(bytes.Buffer), &savedObjects, []int{200}); err != nil {
		return err
	}

	if patternExists(pattern, &savedObjects) {
		log.Printf("Skip create index pattern %q for space %q as it already exists", pattern, spaceName)
		return nil
	}

	log.Printf("Create index pattern %q for space %q", pattern, spaceName)
	savedObject := SavedObject{
		Attributes: map[string]string{
			"title":         pattern,
			"timeFieldName": "@timestamp",
		},
	}
	newPatternRequest := fmt.Sprintf("s/%s/api/saved_objects/index-pattern", spaceName)
	return kibanaClient.doJSONRequest(http.MethodPost, newPatternRequest, objectToBuffer(savedObject), nil, []int{200})
}

func patternExists(pattern string, savedObjects *SavedObjects) bool {
	for _, existingObject := range savedObjects.SavedObjects {
		if existingObject.Attributes["title"] == pattern {
			return true
		}
	}
	return false
}
