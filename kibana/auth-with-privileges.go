package kibana

import (
	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
)

// This method supports privileges defined in the customer YAML.
func (kibanaClient *Client) ConfigureRolesWithPrivileges(es *elasticsearch.Client, space *config.KibanaSpace, oidcRealm string, commonIndexes []string, clusterPrivileges []string, publicDatastreams []string, skipAddingWildcardsInPatterns bool) error {

	// loop over roles within the space and create roles and rolemappings accordingly
	for _, role := range space.Roles {

		privileges := role.Privileges

		// if no privileges are given use only the 'read' permission
		if len(privileges) == 0 {
			privileges = []string{"read"}
		}

		err := kibanaClient.createRole(space.Name, role.RoleName, space.IndexPatterns, privileges, commonIndexes, clusterPrivileges, es, publicDatastreams, skipAddingWildcardsInPatterns)
		if err != nil {
			return err
		}

		if oidcRealm != "" {
			err := createRoleMapping(es, oidcRealm, role.RoleName, role.OidcGroups)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
