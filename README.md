# Kibana/Elastic command line tools

Purpose of this tool is to automate configuration of Kibana and Elastic. 

The tool supports multiple commands using a subcommands approach. 

# How to use 
`kibana-cli <subcommand>`

You can also run the tool in a docker image:
```
IMAGE=registry.gitlab.com/logius/cloud-native-overheid/components/kibana-cli:latest
docker run --rm \
    -v $PWD/examples:/examples \
    $IMAGE configure-elastic --config=examples/tenant.config.yaml
```

The following subcommands are available:
| Subcommand        |     Description      | 
| :------------- |:-------------| 
  configure-elastic |  Configure Elastic and Kibana Space | 

# Configure Elastic 

The `configure-elastic` subcommand initializes a Kibana space for a tenant or customer.  
Roles are configured in the Kibana space with an optional mapping to OIDC groups.  
Index patterns are created for the Datastreams of this tenant.  
Datastreams and Jaeger indexes are created in Elastic. 
ServiceAccounts in GitLab are prepared to allow users to access Elastic with grafana and fluent. 

Commandline variables:

| Variable        |     Description      | 
| :------------- |:-------------| 
| common-index | Index for which the customer will get read access |
| config |  Path to customer configfile |
| elastic-url | Elastic API URL |
| gitlab-url | Gitlab API URL |
| kibana-url  | Kibana FQDN including protocol, e.g. https://kibana.sample.com  |   
| production    | Boolean to indicate production config |    
| public-datastreams | datastreams readable with all serviceaccounts |
| realm      | OIDC realm     |  
| service-account-suffix | Suffix for CI/CD variables in GitLab with the service account credentials |
| standard_roles | Create standard customer roles |
| rotate-password | Rotate service account password if service account already exists |


For an example of the customer config file see the examples folder. 

## Environment variables
| Variable        |     Description      | 
| :------------- |:-------------| 
  ELASTIC_USERNAME | Kibana/Elastic username |
| ELASTIC_PASSWORD | Elastic password      |   


## Cross Cluster replication

Cross Cluster Replication may be enabled by setting the parameter `CrossClusterReplication`. 

A follower pattern will be created in Elastic for all datastreams of the customer. Elastic will start replicating the backing indices from Datastreams of this customer. Note this is only done for newly created indices. 

Elastic does not rename the Datastreams and backing indices while replicating. This gives errors when the same Datastream already exists in the follower cluster. This is a known issue and might be changed in the future. The workaround is to make sure Datastreams have a globally unique name. The parameter `datastream-suffix` (with a value of e.g. "-az1") is added to the name of the datastream to make the name globally unique. 

# Service accounts
The CLI creates one or two service accounts per customer in Elastic:
* `${CUSTOMER}_readonly_service_account`: Read-only permissions to the data streams and indices defined in `config.elastic-*.datastreams[]` and `config.elastic-*.kibana-spaces[].index_patterns[]`; also possibility to create API keys.
* `${CUSTOMER}_fluentd_service_account`: Optional, skipped when argument `skip-fluentd-service-account` is set to true; account for Fluentd (Banzai) to insert documents in the data streams and indices defined in `config.elastic-*.datastreams[]` and `config.elastic-*.kibana-spaces[].index_patterns[]`.
* `skip-adding-wildcards-in-patterns` default false. Do not add wildcards on index or datastream patterns.

API key creation cannot be done in the GUI, only via the Elastic API with a call like this:

```
curl -u ${USER}:${PASSW} -X POST "${ELASTIC_URL}/_security/api_key?pretty" -H 'Content-Type: application/json' -d'
{
  "name": "my-api-key",
  "expiration": "1d"   
  }
}'
```

# Upgrade from 0.8 to 0.9

In v0.9 the commands for kibana and elastic are merged in a single command. 
The configuration for elastic and kibana has changed: 
- kibana-spaces is now a child block of elastic. 
- elastic now had two blocks: elastic-prod and elastic-test. 
