package config

import (
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// Namespace defines the K8s namespace for a customer.
type Namespace struct {
	Name string
}

// Group defines the oidc groups for a customer
type Group struct {
	Name string `validate:"required"`
}

type Role struct {
	RoleName string `validate:"required" yaml:"name"`
}

type CfgRole struct {
	RoleName   string   `validate:"required" yaml:"name"`
	OidcGroups []string `validate:"required" yaml:"groups"`
	Privileges []string `yaml:"privileges"`
}

type KibanaSpace struct {
	Name          string    `validate:"required"`
	Roles         []CfgRole `validate:"required,dive"`
	IndexPatterns []string  `validate:"required" yaml:"index_patterns"`
}

type CfgDataStream struct {
	Name   string `validate:"required"`
	Policy string // default short
}
type CfgElastic struct {
	CrossClusterReplication bool            `yaml:"crossClusterReplication"`
	DataStreams             []CfgDataStream `validate:"required,dive"`
	JaegerPrefixes          []string        `yaml:"jaegerPrefixes"`
	KibanaSpaces            []KibanaSpace   `yaml:"kibana-spaces" validate:"required"`
	IngestPipelines			[]string 		`yaml:"ingestPipelines"`
}

// GitLabGroup specifies a group in GitLab
type GitLabGroup struct {
	Name string `validate:"required"`
}

// GitLab configuration
type GitLab struct {
	Groups []GitLabGroup `validate:"required,dive"`
}

// Customer configuration
type Customer struct {
	Name        string
	Groups      []Group     `validate:"required,dive"`
	GitLab      GitLab      `validate:"required"`
	ElasticTest *CfgElastic `yaml:"elastic-test" validate:"required"`
	ElasticProd *CfgElastic `yaml:"elastic-prod"`
}

// Config contains customer configuration
type Config struct {
	Customer Customer `validate:"required"`
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		errors.Errorf("Error reading YAML file: %s\n", err)
	}

	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer, nil
}
