package elastic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"strconv"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

type flags struct {
	elasticURLAZ1 *string
	elasticURLAZ2 *string
}

// NewCommand creates a new command.
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := checkCCR(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "check-ccr",
		Short: "Check CCR",
		Long:  "Check CCR",
	}

	flags.elasticURLAZ1 = cmd.Flags().String("elastic-url-az1", "", "Elastic API Endpoint AZ1")
	flags.elasticURLAZ2 = cmd.Flags().String("elastic-url-az2", "", "Elastic API Endpoint AZ2")

	return cmd
}

func checkCCR(cmd *cobra.Command, flags *flags) error {

	esAZ1, err := createElasticClient(*flags.elasticURLAZ1, "_AZ1")
	if err != nil {
		return err
	}
	esAZ2, err := createElasticClient(*flags.elasticURLAZ2, "_AZ2")
	if err != nil {
		return err
	}

	datastreamStatsAZ1, err := getDataStreams(esAZ1)
	if err != nil {
		return err
	}

	datastreamStatsAZ2, err := getDataStreams(esAZ2)
	if err != nil {
		return err
	}

	fmt.Printf(` 
	4 reports are shown: 
	AZ1 
	- Check if leaders have followers
	- Check if followers have leaders
	AZ2
	- Check if leaders have followers
	- Check if followers have leaders

	Examples:
	jaeger-operator-az1
  		.ds-jaeger-operator-az1-2022.09.07-000001: 2310 ?? missing
	
	This means the above index does not have a follower. 

	jaeger-operator-az1
		.ds-jaeger-operator-az1-2022.09.05-000090: 68233 == 68233 
		.ds-jaeger-operator-az1-2022.09.06-000092: 105529 == 105529 
		.ds-jaeger-operator-az1-2022.09.07-000094: 54210 <> 20 (last index)

	The first two indexes have the same amount of docs in both clusters. This is what we want. 
	The last index is also OK. For a working index elastic does not report the correct nr of docs. 

	postgres-operator-v5-az2
  		.ds-postgres-operator-v5-az2-2022.09.05-000001: 0 ?? missing leader

	This means the follower index does not have a leader. 
	`)

	fmt.Printf("\n## AZ1 Check if leaders have followers\n")
	checkFollowers(datastreamStatsAZ1, datastreamStatsAZ2)
	fmt.Printf("\n## AZ1 Check if followers have leaders\n")
	checkMissingLeaders(datastreamStatsAZ1, datastreamStatsAZ2)

	fmt.Printf("\n## AZ2 Check if leaders have followers\n")
	checkFollowers(datastreamStatsAZ2, datastreamStatsAZ1)
	fmt.Printf("\n## AZ2 Check if followers have leaders\n")
	checkMissingLeaders(datastreamStatsAZ2, datastreamStatsAZ1)

	return nil

}

func checkMissingLeaders(datastreamStatsAZ1 []DataStreamStats, datastreamStatsAZ2 []DataStreamStats) {
	for _, datastream := range datastreamStatsAZ1 {
		if datastream.Leader {
			continue
		}
		leaderStats := getDatastreamStats(datastreamStatsAZ2, datastream.Name, false)

		missingLeader := false
		for _, index := range datastream.Indices {
			var indexStats *IndexStats
			if leaderStats != nil {
				indexStats = getIndexStats(leaderStats, index.IndexName)
			}
			if indexStats == nil {
				if !missingLeader {
					missingLeader = true
					fmt.Printf("%s\n", datastream.Name)
				}
				fmt.Printf("  %s: %d ?? missing leader\n", index.IndexName, index.DocsCount)
				continue
			}
		}
	}
}

func checkFollowers(datastreamStatsAZ1 []DataStreamStats, datastreamStatsAZ2 []DataStreamStats) {
	for _, datastream := range datastreamStatsAZ1 {

		if !datastream.Leader {
			continue
		}
		if datastream.getTotalDocs() == 0 {
			continue
		}
		followerStats := getDatastreamStats(datastreamStatsAZ2, datastream.Name, false)

		lastIndex := datastream.getLastIndex()
		fmt.Printf("%s\n", datastream.Name)
		for _, index := range datastream.Indices {
			var indexStats *IndexStats
			if followerStats != nil {
				indexStats = getIndexStats(followerStats, index.IndexName)
			}
			if indexStats == nil {
				fmt.Printf("  %s: %d ?? missing\n", index.IndexName, index.DocsCount)
				continue
			}
			var op string
			if index.DocsCount == indexStats.DocsCount {
				op = "=="
			} else {
				op = "<>"
			}

			var indicator string
			if lastIndex.getNr() == index.getNr() {
				indicator = "(last index)"
			} else if index.DocsCount != indexStats.DocsCount {
				indicator = "(NOK)"
			}
			fmt.Printf("  %s: %d %s %d %s\n", index.IndexName, index.DocsCount, op, indexStats.DocsCount, indicator)
		}
	}
}

func getDatastreamStats(datastreamStats []DataStreamStats, datastreamName string, leader bool) *DataStreamStats {

	for _, datastream := range datastreamStats {
		if datastream.Name == datastreamName {
			return &datastream
		}
	}
	return nil
}

func getIndexStats(datastreamStats *DataStreamStats, indexName string) *IndexStats {

	for _, index := range datastreamStats.Indices {
		if index.IndexName == indexName {
			return &index
		}
	}
	return nil
}

func (indexStats *IndexStats) getNr() int {
	segments := strings.Split(indexStats.IndexName, "-")
	ind, _ := strconv.Atoi(segments[len(segments)-1])
	return ind
}

func (datastreamStats *DataStreamStats) getLastIndex() *IndexStats {
	var last *IndexStats
	for _, indx := range datastreamStats.Indices {
		if last == nil || indx.getNr() > last.getNr() {
			last = &indx
		}
	}
	return last
}

func (datastreamStats *DataStreamStats) getTotalDocs() int {
	var count int
	for _, indx := range datastreamStats.Indices {
		count += indx.DocsCount
	}
	return count
}

type DataStream struct {
	Name    string `json:"name"`
	Indices []struct {
		IndexName string `json:"index_name"`
		IndexUUID string `json:"index_uuid"`
	} `json:"indices"`
	Generation         int    `json:"generation"`
	Status             string `json:"status"`
	Template           string `json:"template"`
	IlmPolicy          string `json:"ilm_policy"`
	Hidden             bool   `json:"hidden"`
	System             bool   `json:"system"`
	AllowCustomRouting bool   `json:"allow_custom_routing"`
	Replicated         bool   `json:"replicated"`
}

type DataStreamList struct {
	DataStreams []DataStream `json:"data_streams"`
}

type IndicesStats struct {
	Indices map[string]struct {
		UUID   string `json:"uuid"`
		Health string `json:"health"`
		Status string `json:"status"`
		Total  struct {
			Docs struct {
				Count   int `json:"count"`
				Deleted int `json:"deleted"`
			} `json:"docs"`
			Store struct {
				Size int `json:"size_in_bytes"`
			} `json:"store"`
		} `json:"total"`
	} `json:"indices"`
}

type IndexStats struct {
	IndexName string
	DocsCount int
}

type DataStreamStats struct {
	Name    string
	Indices []IndexStats
	Leader  bool
}

func getDataStreams(es *elasticsearch.Client) ([]DataStreamStats, error) {

	res, err := es.Indices.GetDataStream()
	if err != nil {
		return nil, err
	}

	var dataStreamList DataStreamList
	if err := parseResponse(res, &dataStreamList); err != nil {
		return nil, err
	}

	dataStreamStatList := make([]DataStreamStats, 0)
	for _, datastream := range dataStreamList.DataStreams {
		if !strings.HasSuffix(datastream.Name, "-az1") && !strings.HasSuffix(datastream.Name, "-az2") {
			continue
		}

		datastreamStats := DataStreamStats{
			Name:    datastream.Name,
			Leader:  len(datastream.IlmPolicy) > 0, // a follower does not have its own Lifecycle Policy
			Indices: make([]IndexStats, 0),
		}

		for _, index := range datastream.Indices {
			res, err := es.Indices.Stats(es.Indices.Stats.WithIndex(index.IndexName), es.Indices.Stats.WithForbidClosedIndices(false), es.Indices.Stats.WithMetric("docs,flush,refresh"))
			if err != nil {
				return nil, err
			}

			var stats IndicesStats
			if err := parseResponse(res, &stats); err != nil {
				return nil, err
			}
			for key, index := range stats.Indices {
				datastreamStats.Indices = append(datastreamStats.Indices, IndexStats{
					IndexName: key,
					DocsCount: index.Total.Docs.Count,
				})

			}
		}

		dataStreamStatList = append(dataStreamStatList, datastreamStats)
	}

	return dataStreamStatList, nil
}

func parseResponse(res *esapi.Response, response interface{}) error {
	defer res.Body.Close()
	if res.IsError() {
		return fmt.Errorf("%v", res)
	}
	responseData, err := ioutil.ReadAll(res.Body)

	//fmt.Printf("%v", string(responseData))

	if err != nil {
		return fmt.Errorf("unable to perform action readAll: %w", err)
	}

	return json.Unmarshal(responseData, response)
}

func createElasticClient(elasticURL string, suffix string) (*elasticsearch.Client, error) {
	elasticUser := os.Getenv("ELASTIC_USERNAME" + suffix)
	elasticPassword := os.Getenv("ELASTIC_PASSWORD" + suffix)

	if elasticUser == "" {
		return nil, errors.Errorf("Missing environment variable ELASTIC_USERNAME")
	}
	if elasticPassword == "" {
		return nil, errors.Errorf("Missing environment variable ELASTIC_PASSWORD")
	}
	cfg := elasticsearch.Config{
		Addresses: []string{elasticURL},
		Username:  elasticUser,
		Password:  elasticPassword,
	}
	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("error creating the elastic client: %s", err)
	}

	return es, nil
}
