package commands

import (
	"github.com/spf13/cobra"
	ccr "gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/ccr"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/elastic"
	ingest "gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/ingest"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "kibana",
		Short: "OPS tools for Elastic and Kibana stack",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(elastic.NewCommand())
	cmd.AddCommand(ingest.NewCommand())
	cmd.AddCommand(ccr.NewCommand())
}
