package elastic

import (
	"fmt"
	"os"
	"strings"
	"io"
	"encoding/json"
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/elastic"
	"github.com/spf13/cobra"
)

type flags struct {
	config                 *string
	source                 *string
	destination            *string
}

// NewCommand creates a new command.
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := syncIngestPipelines(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "sync-ingest-pipelines",
		Short: "Syncs ingest pipelines between Elastic instances",
		Long:  "Syncs ingest pipelines between Elastic instances",
	}

	flags.source = cmd.Flags().String("source", "", "source Elastic API endpoint")
	flags.destination = cmd.Flags().String("destination", "", "destination Elastic API endpoint")
	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("source")
	cmd.MarkFlagRequired("destination")

	return cmd
}

type IngestGetPipelineRequest struct {
	PipelineID string


}

func syncIngestPipelines(cmd *cobra.Command, flags *flags) error {

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	es, err := elastic.CreateElasticClient(*flags.source)
	if err != nil {
		return err
	}

	if (customerConfig.ElasticProd != nil) &&  (customerConfig.ElasticProd.IngestPipelines != nil) && len(customerConfig.ElasticProd.IngestPipelines) > 0 {
		
		var toSync []map[string]io.Reader

		for _, v := range customerConfig.ElasticProd.IngestPipelines{
			
			res, err := es.Ingest.GetPipeline(es.Ingest.GetPipeline.WithPipelineID(v))
			defer res.Body.Close()
			if err != nil {
				return err
			}
			if res.IsError() {
				log.Printf("Error fetching ingest pipeline %q %v ", v, res)
				return nil
			}

			// extract ingest request body from get ingest pipeline response
			var r map[string]json.RawMessage
			err = json.NewDecoder(res.Body).Decode(&r)
			if err != nil {
				return err
			}

			// skip ingest pipeline if description contains 'draft'
			var m map[string]interface{}
			err = json.Unmarshal(r[v], &m)
			if err != nil {
				return err
			}
			
			description := strings.ToLower(m["description"].(string))
			if strings.Contains(description, "draft"){
				log.Printf("Skipping ingest pipeline %q because it is described as draft", v)
				continue
			}

			t := strings.NewReader(string(r[v]))
			toSync = append(toSync, map[string]io.Reader{v: t})
		}

		elasticPassword := os.Getenv("ELASTIC_PASSWORD_PROD")
		os.Setenv("ELASTIC_PASSWORD", elasticPassword)

		es, err := elastic.CreateElasticClient(*flags.destination)
		if err != nil {
			return err
		}

		for _, v := range toSync{
			for id, body := range v {				
				res, err := es.Ingest.PutPipeline(id, body)
				defer res.Body.Close()
				if err != nil {
					return err
				}
				if res.IsError() {
					return fmt.Errorf("%v", res)
				}
				log.Printf("Ingest pipeline %q synchronised from %q to %q", id, *flags.source, *flags.destination)
			}

		}
	}
	return nil
}
